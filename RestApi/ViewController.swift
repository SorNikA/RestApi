//
//  ViewController.swift
//  RestApi
//
//  Created by Никита Сорокин on 16.05.2022.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    let networkManager = NetworkManager()
    var dataSource = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
        networkManager.obtainPosts { [weak self]result in
            switch result {
            case.failure(let error):
                print("Error: \(error.localizedDescription)")
            case.success(let posts):
                self?.dataSource = posts
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }

    }
    
    
//MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "identifier", for: indexPath)
        let post = dataSource[indexPath.row]
        cell.textLabel?.text = post.title
        cell.detailTextLabel?.text = post.body
        return cell
    }

}

