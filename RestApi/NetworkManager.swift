//
//  NetworkManager.swift
//  RestApi
//
//  Created by Никита Сорокин on 16.05.2022.
//

import Foundation

enum ObtainPostResult {
    case success(posts: [Post])
    case failure(error: Error)
}

class NetworkManager {
    
    let sessionConfiguration = URLSessionConfiguration.default
    let session = URLSession.shared
    let decoder = JSONDecoder()
    
    func obtainPosts(completion: @escaping(ObtainPostResult) -> Void) {
        
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else {
            return
        }

        session.dataTask(with: url) { [weak self](data, response, error) in
            var result: ObtainPostResult
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            guard let strongSelf = self else {
                result = .success(posts: [])
                return
            }
            if error == nil, let parsData = data {
                guard let posts = try? strongSelf.decoder.decode([Post].self, from: parsData) else {
                    result = .success(posts: [])
                    return
                }
                result = .success(posts: posts)
            } else {
                result = .failure(error: error!)
            }
//            completion(result)
        }.resume()
        
    }
}
